describe('Users', () => {
  it('loads', () => {
    cy.visit('http://localhost:8094')
    cy.contains('User List')
  })
  it('loads a list of users', () => {
    cy.visit('http://localhost:8094')
    cy.get('.card-title').should('have.length', 10)
  })
})


